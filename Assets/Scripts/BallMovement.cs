﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BallMovement : MonoBehaviour {

	public float speed;
	public Text countText;
	public Text winText;

	private Rigidbody ball;
	private int count;

	// Use this for initialization
	void Start () {
		ball = GetComponent<Rigidbody> ();
		count = 0;
		setCountText ();
		winText.text = "";
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		ball.AddForce (movement * speed);
	}

	void OnTriggerEnter(Collider collider) {
		if (collider.gameObject.CompareTag("Pickup")) {
			collider.gameObject.SetActive (false);
			count += 1;
			setCountText ();
		}
	}

	void setCountText() {
		countText.text = "Count: " + count.ToString ();
		if (count == 7) {
			winText.text = "Boa Maluketz!";
		}
	}
}
